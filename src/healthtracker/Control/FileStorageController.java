package healthtracker.Control;
import healthtracker.Model.Goal;
import healthtracker.Model.User;
import healthtracker.Model.WeightHistory;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.Iterator;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

/**
 * A class to control the data from users accounts, it deals with converting 
 * java Objects to JSON and saving it to text files. This class also creates
 * Objects from the text files containing JSON data.
 */
public class FileStorageController {
    private final String USERS = "users.txt";
    
    private ArrayList<User> userList;

    public FileStorageController() {
        userList = new ArrayList();
    }
    
    /**
     * Gets the JSON data from a file and creates a list of users with all their
     * relevant data.
     */
    public void readUsersFromFile(){
        JSONParser parser = new JSONParser();
 
        try { 
            Object obj = parser.parse(new FileReader(USERS));
            JSONObject root = (JSONObject) obj;
 
            JSONArray users = (JSONArray) root.get("userList");
            Iterator i = users.iterator();
            while (i.hasNext()) {
                JSONObject user = (JSONObject) i.next();
                userList.add(JSONToUser(user));
            }
 
        } catch (IOException | org.json.simple.parser.ParseException | ParseException e) {
        }        
    }
    
    /**
     * creates a User Object from a JSON user object
     * @param user an individual user in JSON form
     * @return A User Object that contains all the relevant data
     * @throws ParseException
     */
    public User JSONToUser(JSONObject user) throws ParseException{
        String username = (String) user.get("username");
        String name = (String) user.get("fullName");
        String ID = (String) user.get("ID");
        int age = ((Long) user.get("age")).intValue();
        int weight = ((Long)user.get("weight")).intValue();
        String password = (String) user.get("password");
        String email = (String) user.get("email");
        
        JSONArray weightHistory = (JSONArray) user.get("weightHistory");
        ArrayList<WeightHistory> wh = JSONToweightHistoryList(weightHistory);
        
        JSONArray JSONgoals = (JSONArray) user.get("goals");
        ArrayList<Goal> goals = JSONToGoalList(JSONgoals);
        
        User newUser = new User(name, username, ID, age, weight, password, email, wh, goals);
        return newUser;
    }
    
    /**
     * Creates an ArrayList of WeightHistory Objects from a JSON weight history
     * Array.
     * @param ja A JSONArray of weight history JSONObjects
     * @return An ArrayList of WeightHistory Objects
     * @throws ParseException
     */
    public ArrayList<WeightHistory> JSONToweightHistoryList(JSONArray ja) throws ParseException{
        ArrayList<WeightHistory> wh = new ArrayList();
        Iterator i = ja.iterator();
            while (i.hasNext()) {
                JSONObject wehi = (JSONObject) i.next();
                wh.add(JSONToWeightHistoryObject(wehi));
            }
        return wh;
    }
    
    /**
     * A method to create a WeightHistory Object from a JSONObject
     * @param WeHi A JSONObject containing data for a WeightHistory Object
     * @return A WeightHistory Object
     * @throws ParseException
     */
    public WeightHistory JSONToWeightHistoryObject(JSONObject WeHi) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        Date date;
        date = sdf.parse((String) WeHi.get("date"));
        int weight = ((Long)WeHi.get("weight")).intValue();
        WeightHistory wh = new WeightHistory(weight, date);
        return wh;               
    }
    
    /**
     * Creates an ArrayList of Goal Objects from a JSONArray
     * @param ja The JSONArray containing the Goal data
     * @return an ArrayList of Goals
     * @throws ParseException
     */
    public ArrayList<Goal> JSONToGoalList(JSONArray ja) throws ParseException{
        ArrayList<Goal> goals = new ArrayList();
        Iterator i = ja.iterator();
            while (i.hasNext()) {
                JSONObject goal = (JSONObject) i.next();
                goals.add(JSONToGoalObject(goal));
            }
        return goals;
    }   
    
    /**
     * A method to create a Goal object from a JSONObject
     * @param JSONGoal The JSONObject containing data for a Goal Object
     * @return A Goal Object
     * @throws ParseException
     */
    public Goal JSONToGoalObject(JSONObject JSONGoal) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        Date date;
        date = sdf.parse((String) JSONGoal.get("date"));
        int weight = ((Long)JSONGoal.get("weight")).intValue();
        Goal goal = new Goal((String) JSONGoal.get("name"),
                (String) JSONGoal.get("ID"), weight, date,
                Boolean.parseBoolean((String) JSONGoal.get("achieved")));
        return goal;               
    }
    
    /**
     * A method that saves this userList to file in JSON format
     */
    public void writeUsersToFile() {
        JSONObject root = new JSONObject();
        JSONArray users = new JSONArray();
        
        for (User user : userList) {
            users.add(userToJSON(user));
        }

        root.put("userList", users);        
        writeObjectToFile(root);
    }
    
    /**
     * A method to create a JSONObject from a User
     * @param user The user to be translated to JSONObject
     * @return The JSONObject taken from the User
     */
    public JSONObject userToJSON(User user){
        JSONObject newUser = new JSONObject();
        newUser.put("username", user.getUsername());
        newUser.put("fullName", user.getName());
        newUser.put("ID", user.getID());
        newUser.put("age", user.getAge());
        newUser.put("weight", user.getWeight());
        newUser.put("password", user.getPassword());
        newUser.put("email", user.getEmail());
        newUser.put("weightHistory", writeWeightHistoryListToJSON(user));
        newUser.put("goals", writeGoalListToJSON(user));
        return newUser;
    }
    
    /**
     * A method to take a users weight history and create a JSONArray from it
     * @param user The user who's weight history is being translated
     * @return the JSONArray of weight history objects
     */
    public JSONArray writeWeightHistoryListToJSON(User user) {
        JSONArray whList = new JSONArray();       
        for (WeightHistory weHi : user.getHistory()) {
            whList.add(weightHistoryObjectToJSON(weHi));
        }
        return whList;
    }    
    
    /**
     * A method to create a JSONObject from one WeightHistory Object
     * @param weHi the WeightHistory Object to be translated
     * @return a JSONObject containing one WeightHistory Objects data
     */
    public JSONObject weightHistoryObjectToJSON(WeightHistory weHi){
        JSONObject wh = new JSONObject();
        wh.put("weight", weHi.getWeight());
        wh.put("date", weHi.getDate() + "");
        return wh;
    }  
    
    /**
     * A method to take a users goals and create a JSONArray from it
     * @param user The user who's goals are being translated
     * @return the JSONArray of goal objects
     */
    public JSONArray writeGoalListToJSON(User user) {
        JSONArray goalList = new JSONArray();       
        for (Goal goal : user.getGoals()) {
            goalList.add(goalObjectToJSON(goal));
        }
        return goalList;
    }  
    
    /**
     * A method to create a JSONObject from one Goal Object
     * @param goal the Goal Object to be translated
     * @return a JSONObject containing one Goal Objects data
     */
    public JSONObject goalObjectToJSON(Goal goal){
        JSONObject newGoal = new JSONObject();
        newGoal.put("name", goal.getName() + "");
        newGoal.put("ID", goal.getID() + "");
        newGoal.put("weight", goal.getGoalWeight());
        newGoal.put("date", goal.getDeadline() + "");
        newGoal.put("achieved", goal.isAchieved() + "");
        return newGoal;
    }
    
    /**
     * A method to save a JSONObject to a text file
     * @param obj The JSONObject to be written to file
     */
    public void writeObjectToFile(JSONObject obj) {
        try (FileWriter file = new FileWriter(USERS)) {
                file.write(obj.toJSONString());
        } catch (IOException ex) {
            Logger.getLogger(FileStorageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @return this list of Users
     */
    public ArrayList<User> getUserList() {
        return userList;
    }
    
    /**
     *
     * @param u The User to be added
     */
    public void addToUsers(User u){       
        userList.add(u);               
    }    
}
