package healthtracker.Control;

import healthtracker.Control.FileStorageController;
import healthtracker.Model.Goal;
import healthtracker.View.LoginGUI;
import healthtracker.View.ProfileGUI;
import healthtracker.Model.User;
import java.io.IOException;
import java.util.Date;

/**
 * A class to control the model classes of this project and convey them to the 
 * GUI classes to be used by the user
 */
public class ModelController {
    
    private LoginGUI login;
    private ProfileGUI profile;
    private User user;
    private FileStorageController fsc;

    /**
     * A constructor that initialises the GUI and links the 
     * FileStorageController
     * @param fsc The FileStorageController to get the User from
     */
    public ModelController(FileStorageController fsc) {
        login = new LoginGUI(this);
        profile = new ProfileGUI(this);
        this.fsc = fsc;
    }
    
    /**
     * A method that searches for a matched user name and password in the list
     * of users in the FileStorageController
     * @param username The username to search for
     * @param password The password for the User
     * @return true if the details are matched in false if not
     */
    public boolean login(String username, String password){
        for(User itUser : fsc.getUserList()) {
            if(itUser.getUsername().equals(username) 
                    && itUser.getPassword().equals(password)) {
                this.user = itUser;
                return true;
            }
        }
        return false;
    }  
    
    /**
     * Checks whether a user name already exists
     * @param username The username to be checked
     * @return true if the username exists false if not
     */
    public boolean checkIfUsernameExists(String username){
        for(User itUser : fsc.getUserList()) {
            if(itUser.getUsername().equals(username)) {
                this.user = itUser;
                return true;
            }
        }
        return false;
    }      
    
    /**
     * A method to log a user out of the application
     */
    public void logOut(){
        //save data
        fsc.writeUsersToFile();
        //replace User with blank for privacy
        user = new User();
        closeProfile();
        openLogin();
    }

    /**
     * 
     * @return The current user
     */
    public User getUser() {
        return user;
    }

    /**
     * Changes the current User
     * @param user The User to make current
     * @throws IOException
     */
    public void setNewUser(User user) throws IOException {
        this.user = user;
        fsc.addToUsers(user);
        fsc.writeUsersToFile();
    }
    
    /**
     * Saves the Users data to file
     */
    public void saveData(){
        fsc.writeUsersToFile();   
    }
    
    /**
     * Displays the LoginGUI
     */
    public void openLogin(){
        login.setVisible(true);
    }
    
    /**
     * Closes the LoginGUI
     */
    public void closeLogin(){
        login.setVisible(false);
    }
    
    /**
     * Opens the ProfileGUI
     */
    public void openProfile(){
        profile.setVisible(true);
        profile.updateFields();
        profile.showGoals();
    }
    
    /**
     * Closes the ProfileGUI
     */
    public void closeProfile(){
        profile.setVisible(false);
    }
    
    /**
     * Updates a Goal to achieved if it meets the criteria
     * @param goal The Goal to be updated
     */
    public void updateGoalAchieved(Goal goal){
        if( !goal.isAchieved() 
                && goal.getGoalWeight() >= user.getWeight() 
                && goal.getDeadline().after(new Date())){
            goal.setAchieved(true);
        }
    }
}
