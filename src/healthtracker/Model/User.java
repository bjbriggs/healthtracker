package healthtracker.Model;

import java.util.ArrayList;

/**
 * A class to model a User which has information about someone's weight and
 * historical weight, it also has a list of Goals which are set by the user
 */
public class User {
    private String name;
    private String username;
    private String ID;
    private int age;
    private int weight;
    private String password;
    private String email;
    private ArrayList<WeightHistory> history;
    private ArrayList<Goal> goals;  

    /**
     * A constructor for a User where there is no available weight history 
     * or goal list
     * @param name The full name of the User
     * @param username The Username of this User
     * @param ID The unique identifier for this User
     * @param age The age of this User
     * @param weight The current Weight of the User
     * @param password The Users password
     * @param email The Users email address
     */
    public User(String name, String username, String ID, int age, int weight,
            String password, String email){
        this.name = name;
        this.username = username;
        this.ID = ID;
        this.age = age;
        this.weight = weight;
        this.password = password;
        this.email = email;
        history = new ArrayList();
        goals = new ArrayList();
    }
    
    /**
     *
     * @param name The full name of the User
     * @param username The Username of this User
     * @param ID The unique identifier for this User
     * @param age The age of this User
     * @param weight The current Weight of the User
     * @param password The Users password
     * @param email The Users email address
     * @param history A list of previously recorded weights
     * @param goals A list of User set Goals
     */
    public User(String name, String username, String ID, int age, int weight,
            String password, String email, ArrayList<WeightHistory> history,
            ArrayList<Goal> goals){
        this.name = name;
        this.username = username;
        this.ID = ID;
        this.age = age;
        this.weight = weight;
        this.password = password;
        this.email = email;
        this.history = history;
        this.goals = goals;
    }      
    
    /**
     * A constructor for a blank User
     */
    public User(){
        this.name = "";
        this.username = "";
        this.ID = "";
        this.age = 0;
        this.weight = 0;
        this.password = "";
        this.email = "";
        history = new ArrayList();
        goals = new ArrayList();
    }
    
    /**
     * A method to add a new weight to the Users history
     * @param w the new weight to be added
     */
    public void addToWeightHistory(WeightHistory w){
        history.add(w);
    }
    
    /**
     * A method to add a new Goal to this Users list of Goals
     * @param goal the Goal to be added
     */
    public void addToGoals(Goal goal){
        goals.add(goal);
    }

    /**
     *
     * @return
     */
    public ArrayList<Goal> getGoals() {
        return goals;
    }
    
    /**
     *
     * @return
     */
    public ArrayList<WeightHistory> getHistory() {
        return history;
    }
    
    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getID() {
        return ID;
    }

    /**
     *
     * @param ID
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     *
     * @return
     */
    public int getAge() {
        return age;
    }

    /**
     *
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     *
     * @return
     */
    public int getWeight() {
        return weight;
    }

    /**
     *
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", username=" + username + ", ID=" 
                + ID + ", age=" + age + ", weight=" + weight + ", password=" 
                + password + ", email=" + email + ", history=" + history 
                + ", goals=" + goals + '}';
    }  
}
