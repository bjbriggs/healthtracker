package healthtracker.Model;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * A class to contain and control details about a user set goal where the goal
 * is to be below a certain weight by a certain date.
 */
public class Goal implements Comparable<Goal>{
    private String name;
    private String ID;
    private int goalWeight;
    private Date deadline;
    private boolean achieved;
    
    /**
     *
     * @param name The given name of the Goal
     * @param ID a unique identifier given to the Goal
     * @param goalWeight the weight the user is trying to get to
     * @param deadline the date by which the user is to be below the goal weight
     * @param achieved whether the Goal has been reached or not
     */
    public Goal(String name, String ID, int goalWeight, Date deadline,
            boolean achieved) {
        this.name = name;
        this.ID = ID;
        this.goalWeight = goalWeight;
        this.deadline = deadline;
        this.achieved = achieved;
    }
       
    /**
     *
     * @return The deadline date in String form
     */
    public String getDateAsString(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(deadline);    
    }
    
    /**
     *
     * @return the Goal name
     */
    public String getName() {
        return name;
    }
    
    /**
     *
     * @return whether the deadline date has passed or not
     */
    public boolean isCurrent() {
        return deadline.after(new Date());
    }
    
    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getID() {
        return ID;
    }

    /**
     *
     * @param
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     *
     * @return
     */
    public int getGoalWeight() {
        return goalWeight;
    }

    /**
     *
     * @param goalWeight
     */
    public void setGoalWeight(int goalWeight) {
        this.goalWeight = goalWeight;
    }

    /**
     *
     * @return
     */
    public Date getDeadline() {
        return deadline;
    }

    /**
     *
     * @param deadline
     */
    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    /**
     *
     * @return
     */
    public boolean isAchieved() {
        return achieved;
    }

    /**
     *
     * @param achieved
     */
    public void setAchieved(boolean achieved) {
        this.achieved = achieved;
    }

    @Override
    public String toString() {
        return "Goal{" + "name=" + name + ", ID=" + ID 
                + ", goalWeight=" + goalWeight + ", deadline=" 
                + deadline + ", achieved=" + achieved + '}';
    }

    @Override
    public int compareTo(Goal o) {
        return this.deadline.compareTo(o.getDeadline());
    }
}
