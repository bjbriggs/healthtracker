package healthtracker.Model;
import healthtracker.Control.ModelController;
import healthtracker.Control.FileStorageController;
import java.io.IOException;

/**
 * The main class of this project which is a health tracker for users to track
 * their weight and set goals for the future
 */
public class HealthTracker {
    /**
     * A main method to start the program from, it reads the saved data an
     * initialises the controllers which then control the rest of the flow
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        FileStorageController f = new FileStorageController();
        f.readUsersFromFile();
        
        ModelController modelController = new ModelController(f);
        modelController.openLogin();
    }
}
