package healthtracker.Model;
import healthtracker.Control.ModelController;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Day;

/**
 * A class used to create line graphs that display the users progress on their
 * weight over time.
 */
public class LineGraph extends ApplicationFrame {
    
    ModelController mc;
       
    /**
     * A constructor that creates a chart and displays it to a given JPanel
     * @param xTitle The title to display on the x-axis
     * @param yTitle The title to display on the y-axis
     * @param mc The controller this class takes data from
     * @param jPanel  The panel the chart will be added to
     * @param max
     */
    public LineGraph( String xTitle , String yTitle,
            ModelController mc, JPanel jPanel, boolean max) {
        super(xTitle);
        this.mc = mc;
        JFreeChart lineChart= ChartFactory.createLineChart(
        yTitle,"Date","Weight (KG)",createDataset(max),
        PlotOrientation.VERTICAL,
        true,true,false);

        //display chart to given JPanel
        ChartPanel chartPanel = new ChartPanel( lineChart );
        jPanel.setLayout(new java.awt.BorderLayout());
        jPanel.add(chartPanel,BorderLayout.CENTER);
        jPanel.validate();
    }    
    
    private DefaultCategoryDataset createDataset(boolean max) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        //create data set from the current Users weight history
        ArrayList<WeightHistory> whList = mc.getUser().getHistory();
        if(!max){
            ArrayList<WeightHistory> whListNew = new ArrayList();
            int clipStart;
            //check there are at least 5 entries available
            if(whList.size() < 5){
                clipStart = 0;
            }else{
                clipStart = whList.size() - 5;
            }
            //ridiculous date for first iteration
            Day tempDay = new Day(1,1,1901);
            for(int i = clipStart; i < whList.size(); i++){   
                //if there are entries on the same day make sure five
                //items are still displayed
                if(tempDay.equals(new Day(whList.get(i).getDate()))){
                    //remove the duplicate day
                    whList.remove(i-1);
                    whListNew = new ArrayList();
                    //check if there are still 5 entries available
                    if(whList.size() < 5){
                        clipStart = 0;
                    }else{
                        clipStart = whList.size() - 5;
                    }
                    i = clipStart;
                    tempDay = new Day(1,1,1901);
                }else{
                    whListNew.add(whList.get(i));
                    tempDay = new Day(whList.get(i).getDate());
                }
            }
            whList = whListNew;
        }
        for (WeightHistory wh : whList) { 
            dataset.addValue((double)wh.getWeight(), "",
                    "" + new Day(wh.getDate()));
        }
                    
        return dataset;
    }   
}