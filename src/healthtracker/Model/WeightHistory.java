package healthtracker.Model;

import java.util.Date;

/**
 * A class to model a Users historical weight
 */
public class WeightHistory {
    private int weight;
    private Date date;

    @Override
    public String toString() {
        return "Weight was: " + weight + "KG on " + date;
    }

    /**
     *
     * @param weight The weight of the User
     * @param date The Date the weight was recorded
     */
    public WeightHistory(int weight, Date date) {
        this.weight = weight;
        this.date = date;
    }

    /**
     *
     * @return
     */
    public int getWeight() {
        return weight;
    }

    /**
     *
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }        
}
